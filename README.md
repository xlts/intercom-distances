# Intercom-Distances

### Prerequisites

Ruby 2.6.1 (https://www.ruby-lang.org/en/documentation/installation/)

### Running the program

`ruby app.rb`

Results will be stored to `output.txt`.

### Running tests

Run the entire test suite with `ruby test/run_all.rb`.

Individual tests can be ran with `ruby test/<path_to_test_file>`.

### Open question: Proudest Achievement

> What's your proudest achievement? It can be a personal project or something you've worked on professionally. Just a short paragraph is fine, but I'd love to know why you're proud of it, what impact it had (If any) and any insights you took from it.

One thing that comes to my mind is a fairly involving optimization plan that me and my team had to figure out and implement in one project. The system processed large volumes of near-realtime data that was produced by users clicking elements of web pages. Whenever a certain metric had to be pulled (e.g. "what's the average time spent on given pages on a given website by users from country X"), we would query Elasticsearch where all of the raw tracking data was stored. However, despite the cluster being large, the queries started to perform poorly, and we noticed an impact on other core functionalities of the system, also depending on Elasticsearch health.

The project was running on AWS infrastructure, so I started off by looking up possible services that we could use and integrate within our system. Knowing that we have a clearly-defined set of metrics to pull, I figured out that DynamoDB can be used, serving as an equivalent to the "classic" materialized views from RDBM systems. The solution allowed us to set up a flow in which the database was being populated with website traffic data that was independent from the "core" application logic. We ended up creating multiple DynamoDB tables, each one providing integrated data for a given metric, that are used throughout the system, both in backend business logic as well as in user interfaces.

While this was nothing spectacular, I was very happy to bring a safe, important optimization to a large, running system. The experience definitely made me learn a lot not only about AWS components, but also opened my mind to the fact that unorthodox solutions that sometimes have to be implemented, at the expense of concepts like data redundancy or "self-containedness" of an IT system.

