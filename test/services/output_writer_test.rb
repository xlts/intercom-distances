require_relative "../test_helper"

class OutputWriterTest < Test::Unit::TestCase
  def setup
    @file_path = File.join(File.dirname(__FILE__), "../data/output.txt")
  end

  def teardown
    File.delete(@file_path) if File.exists?(@file_path)
  end

  def test_write_to_file
    OutputWriter.write_to_file(
      file_path: @file_path,
      users: [
        User.new(latitude: 53.33412, longitude: -5.12340, id: 5, name: "John Doe"),
        User.new(latitude: -15.12213, longitude: 56.53889, id: 3, name: "Jane Doe")
      ]
    )

    File.open(@file_path, "r") do |file|
      assert_equal(
        "{\"name\":\"John Doe\",\"user_id\":5}\n" \
        "{\"name\":\"Jane Doe\",\"user_id\":3}\n",
        file.read
      )
    end
  end
end
