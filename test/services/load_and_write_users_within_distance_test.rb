require_relative "../test_helper"

class LoadAndWriteUsersWithinDistanceTest < Test::Unit::TestCase
  def setup
    @input_file_path = File.join(File.dirname(__FILE__), "../data/customers.txt")
    @output_file_path = File.join(File.dirname(__FILE__), "../data/output.txt")
    @location_from = Location.new(latitude: 53.1489345, longitude: -6.8422408)
    @max_distance_km = 150
  end

  def teardown
    File.delete(@output_file_path) if File.exists?(@output_file_path)
  end

  def test_call
    LoadAndWriteUsersWithinDistance.call(
      input_file_path: @input_file_path,
      output_file_path: @output_file_path,
      location_from: @location_from,
      max_distance_km: @max_distance_km
    )

    File.open(@output_file_path, "r") do |file|
      assert_equal(
        "{\"name\":\"Test User 3\",\"user_id\":2}\n" \
        "{\"name\":\"Test User 1\",\"user_id\":3}\n",
        file.read
      )
    end
  end
end
