require_relative "../test_helper"

class UsersLoaderTest < Test::Unit::TestCase
  def setup
    @file_path = File.join(File.dirname(__FILE__), "../data/customers.txt")
  end

  def test_load
    @users = UsersLoader.load(@file_path)

    assert_user_loaded(
      latitude: 53.33412,
      longitude: -5.12340,
      id: 3,
      name: "Test User 1"
    )

    assert_user_loaded(
      latitude: 50.91888,
      longitude: 2.22113,
      id: 5,
      name: "Test User 2"
    )

    assert_user_loaded(
      latitude: 54.08934,
      longitude: -5.67699,
      id: 2,
      name: "Test User 3"
    )
  end

  private

  def assert_user_loaded(latitude:, longitude:, id:, name:)
    user = @users.find do |user|
      user.latitude == latitude &&
        user.longitude == longitude &&
        user.id == id &&
        user.name == name
    end

    assert_not_nil(user)
    assert(user.is_a?(User))
  end
end
