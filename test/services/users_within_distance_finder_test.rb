require_relative "../test_helper"

class UsersWithinDistanceFinderTest < Test::Unit::TestCase
  def setup
    @location_from = Location.new(latitude: 53.1489345, longitude: -6.8422408)

    @users = [
      User.new(latitude: 52.986375, longitude: -6.043701, id: 5, name: "Test User 1"), # 56.33 km away
      User.new(latitude: 51.92893, longitude: -10.27699, id: 2, name: "Test User 2"), # 269 km away
      User.new(latitude: 52.3191841, longitude: -8.5072391, id: 3, name: "Test User 3") # 145.2 km away
    ]

    @max_distance_km = 150
  end

  def test_find_sorted_by_id
    sorted_users_within_distance = UsersWithinDistanceFinder.new(
      users: @users,
      location_from: @location_from,
      max_distance_km: @max_distance_km
    ).find_sorted_by_id

    assert_equal(2, sorted_users_within_distance.size)

    first_user = sorted_users_within_distance[0]
    assert_equal(first_user.id, 3)
    assert_equal(first_user.name, "Test User 3")

    second_user = sorted_users_within_distance[1]
    assert_equal(second_user.id, 5)
    assert_equal(second_user.name, "Test User 1")
  end
end
