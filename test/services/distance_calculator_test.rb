require_relative "../test_helper"

class DistanceCalculatorTest < Test::Unit::TestCase
  def setup
    @calculator = DistanceCalculator.new(
      locatable_from: Locatable.new(latitude: 53.339428, longitude: -6.257664)
    )
  end

  def test_calculate
    assert_equal(
      313.2560763023187,
      @calculator.calculate(
        locatable_to: Locatable.new(latitude: 51.92893, longitude: -10.27699)
      )
    )

    assert_equal(
      788.8365360353591,
      @calculator.calculate(
        locatable_to: Locatable.new(latitude: 54.08934, longitude: 5.67699)
      )
    )

    assert_equal(
      0,
      @calculator.calculate(
        locatable_to: Locatable.new(latitude: 53.339428, longitude: -6.257664)
      )
    )
  end
end
