require_relative "../test_helper"

class DegreesToRadiansConverterTest < Test::Unit::TestCase
  def test_convert
    assert_equal(0.6760872498672474, DegreesToRadiansConverter.convert(38.736946))
    assert_equal(-0.15956995572269775, DegreesToRadiansConverter.convert(-9.142685))
  end
end
