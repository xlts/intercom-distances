require_relative "../test_helper"

class LocatableTest < Test::Unit::TestCase
  def setup
    @locatable = Locatable.new(latitude: 38.736946, longitude: -9.142685)
  end

  def test_latitude
    assert_equal(38.736946, @locatable.latitude)
  end

  def test_longitude
    assert_equal(-9.142685, @locatable.longitude)
  end

  def test_latitude_in_radians
    assert_equal(0.6760872498672474, @locatable.latitude_in_radians)
  end

  def test_longitude_in_radians
    assert_equal(-0.15956995572269775, @locatable.longitude_in_radians)
  end
end
