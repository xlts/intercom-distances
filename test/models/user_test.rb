require_relative "../test_helper"

class UserTest < Test::Unit::TestCase
  def setup
    @user = User.new(
      latitude: 38.736946,
      longitude: -9.142685,
      name: "John Doe",
      id: 11
    )
  end

  def test_is_locatable
    assert_respond_to(@user, :latitude_in_radians)
    assert_respond_to(@user, :longitude_in_radians)
    assert(@user.is_a?(Locatable))
  end

  def test_id
    assert_equal(11, @user.id)
  end

  def test_name
    assert_equal("John Doe", @user.name)
  end

  def test_output_json
    assert_equal("{\"name\":\"John Doe\",\"user_id\":11}", @user.output_json)
  end
end
