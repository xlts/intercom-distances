require_relative "../test_helper"

class LocationTest < Test::Unit::TestCase
  def test_is_locatable
    location = Location.new(latitude: 38.736946, longitude: -9.142685)

    assert_respond_to(location, :latitude_in_radians)
    assert_respond_to(location, :longitude_in_radians)
    assert(location.is_a?(Locatable))
  end

  def test_location_dublin
    dublin = Location::DUBLIN

    assert_equal(dublin.latitude, 53.339428)
    assert_equal(dublin.longitude, -6.257664)
  end
end
