require "./lib/models/locatable"
require "./lib/models/location"
require "./lib/models/user"

require "./lib/services/degrees_to_radians_converter"
require "./lib/services/distance_calculator"
require "./lib/services/users_loader"
require "./lib/services/users_within_distance_finder"
require "./lib/services/output_writer"
require "./lib/services/load_and_write_users_within_distance"

puts "Starting to load file and calculate distances..."

LoadAndWriteUsersWithinDistance.call(
  input_file_path: File.join(File.dirname(__FILE__), "data/customers.txt"),
  output_file_path: File.join(File.dirname(__FILE__), "output.txt"),
  location_from: Location::DUBLIN,
  max_distance_km: 100
)

puts "Finished. Results were stored to output.txt."