class OutputWriter
  def self.write_to_file(file_path:, users:)
    File.open(file_path, "w") do |file|
      users.each do |user|
        file.write(user.output_json + "\n")
      end
    end
  end
end
