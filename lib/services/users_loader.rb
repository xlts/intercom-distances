require "json"

class UsersLoader
  def self.load(file_path)
    File.foreach(file_path).map do |line|
      parsed_line = JSON.parse(line)
      User.new(
        latitude: parsed_line["latitude"].to_f,
        longitude: parsed_line["longitude"].to_f,
        id: parsed_line["user_id"],
        name: parsed_line["name"]
      )
    end
  end
end
