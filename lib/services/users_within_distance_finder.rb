class UsersWithinDistanceFinder
  def initialize(users:, location_from:, max_distance_km:)
    @users = users
    @location_from = location_from
    @max_distance_km = max_distance_km
  end

  def find_sorted_by_id
    users.select do |user|
      distance_calculator.calculate(locatable_to: user) <= max_distance_km
    end.sort_by(&:id)
  end

  private

  attr_reader :users, :location_from, :max_distance_km

  def distance_calculator
    @distance_calculator ||= DistanceCalculator.new(locatable_from: location_from)
  end
end
