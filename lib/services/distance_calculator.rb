class DistanceCalculator
  EARTH_RADIUS_KM = 6371.009

  def initialize(locatable_from:)
    @locatable_from = locatable_from
  end

  def calculate(locatable_to:)
    Math::acos(
      (Math::sin(locatable_from.latitude_in_radians) * Math::sin(locatable_to.latitude_in_radians)) +
      Math::cos(locatable_from.latitude_in_radians) * Math::cos(locatable_to.latitude_in_radians) *
      Math::cos(DegreesToRadiansConverter.convert(locatable_to.longitude - locatable_from.longitude))
    ) * EARTH_RADIUS_KM
  end

  private

  attr_reader :locatable_from
end
