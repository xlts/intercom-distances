class DegreesToRadiansConverter
  def self.convert(degrees)
    degrees * Math::PI / 180.0
  end
end
