class LoadAndWriteUsersWithinDistance
  def self.call(input_file_path:, output_file_path:, location_from:, max_distance_km:)
    users = UsersLoader.load(input_file_path)

    users_within_distance = UsersWithinDistanceFinder.new(
      users: users,
      location_from: location_from,
      max_distance_km: max_distance_km
    ).find_sorted_by_id

    OutputWriter.write_to_file(file_path: output_file_path, users: users_within_distance)
  end
end
