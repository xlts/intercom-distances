class Locatable
  attr_reader :latitude, :longitude

  def initialize(latitude:, longitude:)
    @latitude = latitude
    @longitude = longitude
  end

  def latitude_in_radians
    @latitude_in_radians ||= DegreesToRadiansConverter.convert(latitude)
  end

  def longitude_in_radians
    @longitude_in_radians ||= DegreesToRadiansConverter.convert(longitude)
  end
end
