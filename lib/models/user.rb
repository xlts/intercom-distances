class User < Locatable
  attr_reader :id, :name

  def initialize(latitude:, longitude:, id:, name:)
    super(latitude: latitude, longitude: longitude)

    @id = id
    @name = name
  end

  def output_json
    { name: name, user_id: id }.to_json
  end
end
